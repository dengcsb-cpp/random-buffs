#include "Stats.h"

void addStats(Stats & source, const Stats & toAdd)
{
	source.hp += toAdd.hp;
	source.power += toAdd.power;
	source.vitality += toAdd.vitality;
	source.dexterity += toAdd.dexterity;
	source.agility += toAdd.agility;
}

void printStats(const Stats & stats)
{
	cout << "Hp: " << stats.hp << endl;
	cout << "Pow: " << stats.power << endl;
	cout << "Vit: " << stats.vitality << endl;
	cout << "Dex: " << stats.dexterity << endl;
	cout << "Agi: " << stats.agility << endl;
}
