#pragma once
#include <string>
#include "Unit.h"
#include <iostream>

using namespace std;

class Unit;

class Skill
{
public:
	Skill(std::string name);
	~Skill();

	std::string getName();
	Unit* getActor();
	void setActor(Unit* actor);

	virtual void activate(Unit* target);

private:
	std::string mName;
	Unit* mActor;
};

