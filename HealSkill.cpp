#include "HealSkill.h"



// Invoke the constructor of the parent class. This is required if you are not using parameterless constructor for the parent.
HealSkill::HealSkill(string name, int healValue)
	: Skill(name)
{
	mHealValue = healValue;
}

HealSkill::~HealSkill()
{
}

void HealSkill::activate(Unit * target)
{
	// Append the implementation of the parent class by manually calling its function
	Skill::activate(target);

	target->heal(mHealValue);
	cout << target->getName() << " is healed for " << mHealValue << " HP" << endl;
}
