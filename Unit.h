#pragma once
#include <string>
#include "Stats.h"
#include <cstdlib>
#include <iostream>
#include <vector>
#include "Skill.h"

class Skill;

using namespace std;

class Unit
{
public:
	Unit(string name, const Stats& stats);
	~Unit();

	// Getters
	string getName();
	Stats& getStats();
	int getCurrentHp();

	// This allows us to expose the vector without allowing the user to change the contents of the vector. However, they can still use the Skill's function freely.
	const vector<Skill*>& getSkills();

	// We want to control how the vector is modified so we expose this function instead
	void addSkill(Skill* skill);

	bool alive();
	void heal(int amount);
	void displayStatus();

	// I did not add a UseSkill function since the Skill already has an actor (Unit) attached to it. To activate a skill, you must get the skill from the vector first (getSkills()) then call activate.

private:
	string mName;
	Stats mStats;
	int mCurrentHp;
	vector<Skill*> mSkills;
};

